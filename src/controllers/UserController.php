 
<?php
// UserController.php
include '../config/db.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['action'])) {
    $action = $_POST['action'];

    if ($action == 'register') {
        registerUser($_POST['username'], $_POST['password']);
    } elseif ($action == 'login') {
        loginUser($_POST['username'], $_POST['password']);
    }
}

// Función para registrar un nuevo usuario
function registerUser($username, $password) {
    global $conn;
    $password_hash = password_hash($password, PASSWORD_DEFAULT);
    $role = DEFAULT_ROLE;
    $sql = "INSERT INTO users (username, password, role) VALUES ('$username', '$password_hash', '$role')";
    if ($conn->query($sql) === TRUE) {
        echo "Usuario registrado exitosamente";
    } else {
        echo "Error: " . $conn->error;
    }
}

// Función para iniciar sesión
function loginUser($username, $password) {
    global $conn;
    $sql = "SELECT * FROM users WHERE username='$username'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $user = $result->fetch_assoc();
        if (password_verify($password, $user['password'])) {
            $_SESSION['userid'] = $user['id'];
            $_SESSION['role'] = $user['role'];
            header('Location: ../public/index.php');
            exit();
        } else {
            echo "Contraseña incorrecta";
        }
    } else {
        echo "Usuario no encontrado";
    }
}
?>
