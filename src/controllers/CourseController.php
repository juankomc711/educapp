 
<?php
// CourseController.php
include '../config/db.php';
session_start();

if ($_SESSION['role'] != 'profesor') {
    header("Location: ../public/login.php");
    exit();
}

// Función para crear un nuevo curso
function createCourse($title, $description) {
    global $conn;
    $created_by = $_SESSION['userid'];
    $sql = "INSERT INTO courses (title, description, created_by) VALUES ('$title', '$description', '$created_by')";
    if ($conn->query($sql) === TRUE) {
        echo "Curso creado exitosamente";
    } else {
        echo "Error: " . $conn->error;
    }
}

// Función para listar todos los cursos
function listCourses() {
    global $conn;
    $sql = "SELECT * FROM courses";
    $result = $conn->query($sql);
    return $result;
}

// Función para eliminar un curso
function deleteCourse($course_id) {
    global $conn;
    $sql = "DELETE FROM courses WHERE id='$course_id'";
    if ($conn->query($sql) === TRUE) {
        echo "Curso eliminado exitosamente";
    } else {
        echo "Error: " . $conn->error;
    }
}
?>
