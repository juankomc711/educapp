<!DOCTYPE html>
<html>
<head>
    <title>Listar Cursos</title>
</head>
<body>
    <h1>Listar Cursos</h1>
    <ul>
    <?php
    include '../../src/models/Course.php';
    $courses = getAllCourses();
    if ($courses->num_rows > 0) {
        while($row = $courses->fetch_assoc()) {
            echo "<li>" . $row["title"] . " - " . $row["description"] . "</li>";
        }
    } else {
        echo "No se encontraron cursos.";
    }
    ?>
    </ul>
</body>
</html>

