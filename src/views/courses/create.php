 
<!DOCTYPE html>
<html>
<head>
    <title>Crear Curso</title>
</head>
<body>
    <h1>Crear Curso</h1>
    <form action="create_course.php" method="post">
        <label for="title">Título:</label><br>
        <input type="text" id="title" name="title" required><br>
        <label for="description">Descripción:</label><br>
        <textarea id="description" name="description" rows="4" cols="50" required></textarea><br>
        <button type="submit">Crear Curso</button>
    </form>
</body>
</html>
