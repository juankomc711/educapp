 
<?php
// User.php
include '../config/db.php';

// Función para registrar un nuevo usuario
function registerUser($username, $password, $role) {
    global $conn;
    $password_hash = password_hash($password, PASSWORD_DEFAULT);
    $sql = "INSERT INTO users (username, password, role) VALUES ('$username', '$password_hash', '$role')";
    if ($conn->query($sql) === TRUE) {
        return true;
    } else {
        return false;
    }
}

// Función para iniciar sesión
function loginUser($username, $password) {
    global $conn;
    $sql = "SELECT * FROM users WHERE username='$username'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $user = $result->fetch_assoc();
        if (password_verify($password, $user['password'])) {
            return $user;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
?>
