 
<?php
// Course.php
include '../config/db.php';

// Función para crear un nuevo curso
function createCourse($title, $description, $created_by) {
    global $conn;
    $sql = "INSERT INTO courses (title, description, created_by) VALUES ('$title', '$description', '$created_by')";
    if ($conn->query($sql) === TRUE) {
        return true;
    } else {
        return false;
    }
}

// Función para obtener todos los cursos
function getAllCourses() {
    global $conn;
    $sql = "SELECT * FROM courses";
    $result = $conn->query($sql);
    return $result;
}

// Función para eliminar un curso
function deleteCourse($course_id) {
    global $conn;
    $sql = "DELETE FROM courses WHERE id='$course_id'";
    if ($conn->query($sql) === TRUE) {
        return true;
    } else {
        return false;
    }
}
?>
