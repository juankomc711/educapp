 
<?php
// index.php
session_start();
include '../config/config.php';

if (!isset($_SESSION['userid'])) {
    header('Location: login.php');
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>School System</title>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
    <h1>Bienvenido al Sistema Escolar</h1>
    <nav>
        <ul>
            <li><a href="courses.php">Cursos</a></li>
            <li><a href="profile.php">Perfil</a></li>
            <li><a href="logout.php">Cerrar Sesión</a></li>
        </ul>
    </nav>
</body>
</html>
