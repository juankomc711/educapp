 
<?php
// config.php
define('BASE_URL', 'http://localhost/school_system/');
define('DEFAULT_ROLE', 'estudiante');

// Configuración de correo para recuperación de contraseñas
define('MAIL_HOST', 'smtp.example.com');
define('MAIL_USER', 'no-reply@example.com');
define('MAIL_PASS', 'yourpassword');
define('MAIL_PORT', 587);
?>
